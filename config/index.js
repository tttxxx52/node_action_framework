import dotenv from 'dotenv'

dotenv.config()
const config = {
    message: {
        hello: process.env.HELLO_MESSAGE
    }
}

export default config
