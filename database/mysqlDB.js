const mysql = require("mysql");
const env = process.env.NODE_ENV || 'development';
const config = require('../config/config.json')[env];


export class MySqlDB {
     Option(closure){
        let con = mysql.createConnection({
            host: config.host,
            user: config.username,
            password: config.password,
            database: config.database
        });

        con.connect(function (err) {
            if (err) {
                console.log('connecting error');
                return;
            }
            console.log('connecting success');
        });

        closure(con);
        con.end();
    }
}