export class MySqlTestDB {
    constructor(model) {
        this.where = [];
        this.select = [];
        this.join = [];
        this.model = model.toString()
    }

    //[]string
    Select(arrayString) {
        if (arrayString.isArray) {
            this.select = this.select.concat(arrayString)
        } else {
            console.log("Select value is not array");
        }
    }

    // Where("id","=",12)
    Where(column, decide, value) {
        column = column.toString();
        decide = decide.toString();
        if (column.search(".") === -1) {
            column = this.model + column
        }

        let obj = {
            "column": column,
            "decide": decide,
            "value": value,
        };
        this.where.push(obj)
    }

    // InnerJoin("userWallet","userWallet.user_id","=","user.id")
    InnerJoin(joinTable , column1 , operator , column2 ){
        joinTable = joinTable.toString();
        column1 = column1.toString();
        column2 = column2.toString();
        operator = operator.toString();
        if (column1.search(".") === -1) {
            column1 = this.model + column1
        }
        if (column2.search(".") === -1) {
            column2 = this.model + column2
        }

        let obj = {
            "joinTable": joinTable,
            "column1": column1,
            "column2": column2,
            "operator": operator,
        };
        this.join.push(obj)
    }

    Get(func){
        let con = mysql.createConnection({
            host: config.host,
            user: config.username,
            password: config.password,
            database: config.database
        });

        con.connect(function (err) {
            if (err) {
                console.log('connecting error');
                return;
            }
            console.log('connecting success');
        });

        let sql = "SELECT ";
        

        con.query('SELECT * FROM Users', "", function (err, rows) {
            func(err , rows)
        });

        con.end();
    }



}