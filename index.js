import Koa from 'koa'
import koaBody from 'koa-body'
import middleware from './middlewares/middleware'
const Router = require('koa-router');
const cors = require('@koa/cors');

require('./factory/action_factory');

// koa router
const router = new Router();
const app = new Koa();
// 請注意註冊的順序
app.use(koaBody());
// logger middleware from middleware
app.use(cors());
app.use(middleware);


// router from factory
router.post('/api', ctx => {
    const action = ctx.request.body.action;
    return global["factory"][action](ctx);
});

router.post('/api/auth', ctx => {
    const action = ctx.request.body.action;
    return JSON.stringify(global["authFactory"][action](ctx));
});
app.use(router.routes()).use(router.allowedMethods());

app.listen(9999, '0.0.0.0', () => {
    console.log("server start port:9999")
});