import crypto from '../util/crypto'

export default async (ctx, next) => {
    let token = ctx.request.headers["auth-token"];
    let status
    if (ctx.request.url === "/api/auth" && (token === null || token ==="" || token ===undefined)){
        ctx.status= 401
    }

    if (token !== "" && token !== undefined){
        // 解析 Token
        status = ParseToken(ctx,token)
    }
    if (!status){
        ctx.status= 401
    }else {
        await next() // 請思考next位置的意義
    }
}

function ParseToken(ctx,token) {
    let parseToken= crypto.decryptText(token)
    let parseAry=parseToken.split(";");
    let userId = parseAry[0]
    if (userId !== 0 ){
        ctx.request.body.userId = userId
        return true
    }else {
        return false
    }

}

